Unit cmpunit;

interface

type
    TFT         = Array[1..2] of byte; //position, length

    TMPRec      = Record
      PlNum     : Array[1..7] of char;
      MPs       : Array[1..5] of char;
      MPtype    : char;
      MPcolor   : char;
    end;

    TMPNonRec   = Record
      PlRec     : TMPRec;
      LastN     : Array[1..16] of char;
      FirstN    : Array[1..16] of char;
      City      : Array[1..16] of char;
      state     : Array[1..2] of char;
    end;

    TClubMPRec  = Record
      RecType   : char;
      sequence  : Array[1..4] of char;
      records   : Array[1..4] of char;
      Erating   : Array[1..2] of char;
      GameDate  : Array[1..8] of char;
      ClubSessNum : Array[1..2] of char;
      ClubNum     : Array[1..6] of char;
      SancNum     : Array[1..10] of char;
      UnitNum     : Array[1..3] of char;
      DistNum     : Array[1..2] of char;
      case char of
        '1': (ClubName  : Array[1..40] of char;
              version   : Array[1..12] of char);
        '2': (sessions  : Array[1..2] of char;
              tables    : Array[1..4] of char;
              strata    : char;
              GameType  : char;
              EventName : Array[1..25] of char;
              seniors   : char;
              MPLimits  : Array[1..3,1..4] of char;
              DetRecs   : Array[1..4] of char);
        '3': (Numentries: Array[1..2] of char;
              PlayerRec : Array[1..14] of TMPrec);
        '4': (Nunentries: Array[1..2] of char;
              PlayNonRec: Array[1..3] of TMPNonRec);
        '9': (filler    : Array[1..2] of char;
              ChkMPs    : Array[1..5,1..8] of char);
    end;
    PClubMPRec  = ^TClubMPRec;

const
      RecType   : TFT = (1,1);
      sequence  : TFT = (2,4);
      ClubNum   : TFT = (6,6);
      RepYear   : TFT = (12,4);
      RepMonth  : TFT = (16,2);
        // Rectype 1 definitions follow
              ClubType  : TFT = (18,1);
              UnitNum   : TFT = (19,3);
              DistNum   : TFT = (22,2);
              ClubName  : TFT = (24,40);
              ManagName : TFT = (64,30);
              ClubStreet: TFT = (94,26);
              ClubCity  : TFT = (120,16);
              ClubState : TFT = (136,2);
              ClubZip   : TFT = (138,10);
              CountryCode : TFT = (148,1);
              version   : TFT = (149,12);
        // Rectype 2 definitions follow
              ChargeableGames : TFT = (18,4);
              GameSancFee     : TFT = (22,4);
              GameFees        : TFT = (26,6);
              ChargeableTables: TFT = (32,5);
              TableSancFee    : TFT = (37,4);
              TableFees       : TFT = (41,6);
              LateDays        : TFT = (47,3);
              LateDayRate     : TFT = (50,4);
              LateFees        : TFt = (54,6);
              TotalFees       : TFT = (60,6);
              UnchargedGames  : TFT = (66,4);
              TotalGames      : TFT = (70,4);
              UnchargedTables : TFT = (74,5);
              TotalTables     : TFt = (79,5);
              ReportedSess    : TFT = (84,2);
              PaymentType     : TFt = (86,1);
              CreditCardType  : TFT = (87,1);
              CreditCardNo    : TFT = (88,16);
              ExpiryDate      : TFT = (104,6);
              NameOnCard      : TFT = (110,25);
        // Rectype 3 definitions follow
              SessNum         : TFt = (18,2);
              GameTime        : TFT = (20,5);
              GameType        : TFT = (25,4);
              NumReportedDates: TFT = (29,3);
              NumReportedSecs : TFt = (32,3);
              NumUnrepDates   : TFt = (35,3);
              UnRepDates      : Array[1..5] of TFT = (
                              (38,2),(40,2),(42,2),(44,2),(46,2));
        // Rectype 4 definitions follow
              DSessNum        : TFT = (18,2);
              GameDate        : TFT = (20,8);
              Restriction     : TFt = (28,1);
              Rating          : TFT = (29,2);
              DGameType       : TFt = (31,1);
              Chargeable      : TFT = (32,1);
              Section         : TFt = (33,2);
              EventSession    : TFT = (35,1);
              Tables          : TFt = (36,3);
              Strata          : TFT = (39,1);
              Handicap        : TFt = (40,1);
              MPlimits        : Array[1..3] of TFt = (
                              (41,4),(45,4),(49,4));
              EventName       : TFT = (53,25);
              Director        : TFT = (78,30);
              Comment         : TFt = (108,36);

{const
     RecLen  = 22;
const
     mpftBad = 0;
     mpftOld = 1;
     mpftNew = 2;
     cftype : String[7] = '';

var
   infilename : String;
   infile     : text;
   outfile    : text;
   inpath   : String;
   outfilename : String;}

{function ValidField(const PField: Pointer; const len: byte; Value: String):boolean;
function Bad_Eof(var F: text): boolean;}
function ChrStr(const Rec: String; const Field: TFT): String;
function ChrNum(const Rec: String; const Field: TFT): longint;
function ChrReal(const Rec: String; const Field: TFT; const places: byte): Real;
procedure NumChr(Rec: String; const Field: TFT; const Value: longint);
procedure FixNumField(Rec: String; const Field: TFT);
{procedure closeMPfiles;
function ValidMPFile(var F: text; const filename: String;
         var line: OpenString): byte;}

implementation

uses
    StStrL;

Function Strip(const Field : String) : String;
Var
  Counter : Integer;
  T_Str   : String;
Begin
  t_str := '';
  for Counter := 1 to Length(Field) do
    if Field[Counter] <> ' ' then T_Str := T_Str+Field[Counter];
  Strip := T_Str
End;

Function IVal(const NumStr : String) : LongInt;
Var
  Reslt   : Integer;
  TempVal : LongInt;
  TS      : String;
Begin
  TS := Strip(NumStr);
  if Length(TS) > 0 then begin
    Val(TS,TempVal,Reslt);
    if Reslt > 0 then IVal := 0
    else IVal := TempVal;
  end
  else IVal := 0;
End;

Function Valu(const NumStr : String) : Real;
Var
  Reslt   : Integer;
  TempVal : Real;
  TS      : String;
Begin
  TS := Strip(NumStr);
  if Length(TS) > 0 then begin
    Val(TS,TempVal,Reslt);
    if Reslt > 0 then Valu := 0.0
    else Valu := TempVal;
  end
  else Valu := 0.0;
End;

function numcvt(const num: LongInt; const len: byte;
         const ZeroFill: boolean): String;
begin
  if len < 1 then numcvt := Long2StrL(num)
  else begin
    if not ZeroFill then numcvt := LeftPadL(Long2StrL(num),len)
    else begin
      if num < 0 then numcvt := '-'+LeftPadChL(Long2StrL(Abs(num)),'0',len-1)
      else numcvt := LeftPadChL(Long2StrL(num),'0',len);
    end;
  end;
end;

function ChrNum(const Rec: String; const Field: TFT): longint;
begin
  ChrNum := Ival(Copy(Rec,Field[1],Field[2]));
end;

function ChrReal(const Rec: String; const Field: TFT; const places: byte): Real;
var
   x    : real;
   j    : byte;
begin
  x := ChrNum(Rec,Field);
  for j := 1 to places do x := x/10;
  ChrReal := x;
end;

function ChrStr(const Rec: String; const Field: TFT): String;
begin
  ChrStr := Copy(Rec,Field[1],Field[2]);
end;

(*function ValidMPFile(var F: text; const filename: String;
         var line: OpenString): byte;
var
   valid        : boolean;
   ClubMpRec    : PClubMPRec;
begin
  Assign(F,filename);
  Reset(F);
  {$I-} ReadLn(F,line); {$I+}
  CheckIOResult(filename);
  ClubMPRec := @line[1];
  if Length(line) <> RecLen then with ClubMPRec^ do begin
    valid := (Length(line) > 83) and (RecType = '1')
      and (ChrNum(@sequence,4) = 1) and Valid_club(ChrStr(@ClubNum,6));
    if valid then ValidMPFile := mpftNew
    else begin
      RingBell(false);
      ErrBox(filename+' is not a valid Club MP file',MC,0);
      Close(F);
      ValidMPFile := mpftBad;
    end;
  end
  else ValidMPFile := mpftOld;
end;

procedure closeMPfiles;
begin
  {$I-} Close(infile); {$I+}
  if IOResult = 0 then;
  {$I-} Close(outfile); {$I+}
  if IOResult = 0 then;
end;

procedure BadErr;
begin
  RingBell(false);
  ErrBox(infilename+' is not a valid Club '+Cftype+' file',MC,0);
  CloseMPFiles;
end;

function Bad_Eof(var F: text): boolean;
begin
  Bad_Eof := false;
  if not Eof(F) then exit;
  Bad_Eof := true;
  BadErr;
end;

function ValidField(const PField: Pointer; const len: byte; Value: String):boolean;
begin
  ValidField := true;
  if ChrStr(PField,len) = Value then exit;
  ValidField := false;
  BadErr;
end;*)

procedure NumChr(Rec: String; const Field: TFT; const Value: longint);
var
   TempStr      : String;
begin
  TempStr := numcvt(Value,Field[2],true);
  Move(TempStr[1],Rec[Field[1]],Field[2]);
end;

procedure FixNumField(Rec: String; const Field: TFT);
var
   TempStr      : String;
begin
  TempStr := numcvt(ChrNum(Rec,Field),Field[2],true);
  Move(TempStr[1],Rec[Field[1]],Field[2]);
end;

end.