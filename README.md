# README #
## Clubmail.exe ##

This the primary mail handler for monthly club reports. It was written in Pascal and compiled with Delphi 4 to run on the E: drive at ACBL Hq.

Version 2.0 by David White 15 Oct 2015

Version 1.0 was written by Jim Lopushinshy in 2001

### Quick summary ###

The program read emails sent to club-reports@mail1.acbl.org.

1. Looks for attachments

2. Verifies that the attachments are ACBL club reports

3. Checks the report is allowed

4. Checks that reports are valid

5. Emails confirmation and paysite link to club manager.

Minimum requirements 
Clubmail.exe
Clubmail.ini

### Details ###

As written in 2001 this program includes nine compiler directives. Most of these directives are used for test functions of the program. I would like to remove them as I dislike having a version for testing and a different version for production.

{.$define test} NOT USED IN PRODUCTION.
Program does not work when this directive is active so something is conflicting. This is supposed to count messages and save a report somewhere in the C:\\TEMP\

{.$define usefile} NOT USED IN PRODUCTION.
This is supposed to read a 'message' from a file, then respond to a file. 

{$define clubrep} Used in Production.

{.$define SECOND} NOT USED IN PRODUCTION.

{.$define savebad} NOT USED IN PRODUCTION.

{.$define savenon} NOT USED IN PRODUCTION.

{$define useipnum} Used in Production.

{.$define usedeadbox} NOT USED IN PRODUCTION.

{.$define anyattach} NOT USED IN PRODUCTION.