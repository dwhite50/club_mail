{.$define test}
{.$define usefile}
{$define clubrep}
{.$define second}
{.$define savebad}
{.$define savenon}
{$define useipnum}
{.$define usedeadbox}
{.$define anyattach}
unit mail;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  {Psock, }StdCtrls, StStrL, StStrms,Unit2, ComCtrls, IpSmtp, IpUtils,
  IpSock, IpMsg, IpPop3, cmpunit, CompLHA, ZipMstr;

const
     IniName     = 'ACBLMAIL.INI';
     AppName     = 'GetMail';
     {$ifdef useipnum}
     PopServer   = '192.0.7.8';
     SmtpServer  = '192.0.7.8';
     {$else}
     PopServer   = 'mail1.acbl.org';
     SmtpServer  = 'mail1.acbl.org';
     {$endif}
     EmailSuff   = 'mail1.acbl.org';
     {$ifdef usedeadbox}
     DeadBox     = 'jiml.test';
     {$endif}
     Winmailname = 'WINMAIL.DAT';
     MAttTypes   = 2;
     AttTypes    : Array[1..MAttTypes] of String = ('LZH','ZIP');
     {$ifdef test}
     MsgSaveN     : integer = 0;
     Archivepath = 'C:\TEMP\';
     {$else}
     Archivepath = 'E:\DEPT\550\ACBLSCOR\';
     {$endif}
     ClubSavePath = Archivepath+'REPSAVE\';
     {$ifdef clubrep}
     ProcessSource = '\\ACBL\QDLS\ACBL\';
     AttachPath = Archivepath+'CLUBREP\';
     MailPath = AttachPath+'MAIL\';
     BadAttachPath = AttachPath+'BAD\';
     FinProcessedFile = MailPath+'clbmnth.txt';
     {$endif}
     MaxEmailLen  = 100;

type
  TForm1 = class(TForm)
    Button1: TButton;
    Button2: TButton;
    Button3: TButton;
    StatusBar1: TStatusBar;
    ProgressBar1: TProgressBar;
    SaveDialog1: TSaveDialog;
    IpPop3Client1: TIpPop3Client;
    IpSmtpClient1: TIpSmtpClient;
    CompLHA1: TCompLHA;
    ZipMaster1: TZipMaster;
    StaticText1: TStaticText;
    procedure GetMail(Sender: TObject);
    procedure SetOptions(Sender: TObject);
    procedure Quit(Sender: TObject);
    procedure Pop3ServerResponse(Client: TIpCustomPop3Client;
      ResponseTo: TIpPop3States; const Code, Response: String);
    procedure Pop3TaskComplete(Client: TIpCustomPop3Client;
      Task: TIpPop3Tasks; Response: TStringList);
    procedure ProcessMessage(Client: TIpCustomPop3Client;
      Message: TIpMailMessage);
    procedure MessageSent(Client: TIpCustomSmtpClient;
      var NextMsgReady: Boolean);
    procedure SmtpServerResponse(Client: TIpCustomSmtpClient;
      ResponseTo: TIpSmtpStates; Code: Integer; const Response: String);
    procedure ErrorReply(Sender: TObject; Handle: Cardinal;
      ErrCode: Integer; const ErrStr: String);
    procedure MailClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }
    BodyText : TStringList;
    {$ifndef clubrep}
    AttNewName : TStringList;
    {$endif}
    AttOldName : TStringList;
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

uses
    history,
    YesNoboxU,
    util2,
    UUCode;

{$R *.DFM}

var
   TotalMessages          : integer;
   TotalAttachments       : integer;
   MsgSent                : boolean;
   MsgReceived            : boolean;
   ConnectComplete        : boolean;
   ReplyTextFile : String;
   CopyAttachPath : String;
   {$ifdef clubrep}
   ProcessedFinOK  : boolean;
   Day,Month,Year : word;
   BadMailTo      : String;
   {$else}
   AttachPath    : string;
   {$endif}
   dodelete       : boolean;
   doreply        : boolean;
   dolog          : boolean;
   MailBoxId      : string;
// Add URL for Paysite from .ini
   Paysite        : string;

procedure SetProfile(const pname,Pline: Pchar);
begin
  WritePrivateProfileString(AppName,pname,Pline,
    PChar(AddBackSlashL(JustPathNameL(ParamStr(0)))+IniName));
end;

function GetProfile(const pname, def: Pchar): String;
var
   line   : Array[0..200] of char;
   Pline  : Pchar;
begin
  Pline := line;
  GetPrivateProfileString(AppName,pname,def,Pline,200,
    PChar(AddBackSlashL(JustPathNameL(ParamStr(0)))+IniName));
  GetProfile := Pline;
end;

procedure TForm1.GetMail(Sender: TObject);
var
   j      : integer;
   xNumMsg  : Cardinal;
const
     {$ifdef clubrep}
     Maxmsg = 50;
     {$else}
     Maxmsg = 100;
     {$endif}
begin
  TotalMessages := 0;
  TotalAttachments := 0;
  CopyAttachPath := AddBackSlashL(GetProfile('CopyAttachPath',''));
  {$ifdef clubrep}
  ValidFolder(ArchivePath);
  ValidFolder(ClubSavePath);
  ValidFolder(MailPath);
  ValidFolder(BadAttachPath);
  DecodeDate(Now,Year,Month,Day);
  ErrFileNm := MailPath+'CLUB'+numcvt(Year,4,true)+'.LOG';
  if FileExists(ProcessSource+'clbmnth.txt') then
    CopyFile(ProcessSource+'clbmnth.txt',FinProcessedFile,false);
  {if FileExists(ProcessSource+'clbmp.txt') then
    CopyFile(ProcessSource+'clbmp.txt',MPProcessedFile,false);}
  ProcessedFinOK := FileExists(FinProcessedFile);
//  BadAttachPath := AddBackSlashL(GetProfile('BadAttachPath',''));
  {$ifdef second}
//  SecAttachPath := AddBackSlashL(GetProfile('SecAttachPath',''));
  {$endif}
  BadMailTo := GetProfile('BadMailTo','');
  {$else}
  AttachPath := AddBackSlashL(GetProfile('AttachFilePath',''));
  ErrFileNm := GetProfile('LogFileName','');
  {$endif}
  ValidFolder(AttachPath);
  if not Empty(CopyAttachPath) then ValidFolder(CopyAttachPath);
  ReplyTextFile := GetProfile('ReplyTextFile','');
  MailBoxId := GetProfile('UserID','');
// Read paysite URL from ini
  Paysite := GetProfile('BraintreeURL','');
  dodelete := GetProfile('DeleteOnRead','0') = '1';
  doreply := GetProfile('Reply','1') = '1';
  dolog := Length(ErrFileNm) > 0;
  if dolog then ValidFolder(ErrFileNm);
  if canceled then begin
    Application.Terminate;
    exit;
  end;
  with IpPop3Client1 do begin
    Password := GetProfile('Password','');
    Username := MailBoxId;
    ConnectComplete := false;
    Button2.Enabled := false;
    {Button3.Enabled := false;}
    BodyText := TStringList.Create;
    {$ifndef clubrep}
    AttNewName := TStringList.Create;
    {$endif}
    AttOldName := TStringList.Create;
    ConnectAndLogon(PopServer);
    while not ConnectComplete and not canceled do Application.ProcessMessages;
    if canceled then exit;
    xNumMsg := NumMessages;
    if xNumMsg > MaxMsg then xNumMsg := MaxMsg;
    if xNumMsg > 0 then begin
      {$ifdef test}
      Application.MessageBox(
        PChar('Number or messages '+Long2StrL(NumMessages)),'',MB_OK);
      {$endif}
      IpSmtpClient1.UserID := UserName;
      for j := 1 to xNumMsg do begin
        MsgReceived := false;
        Retrieve(j);
        while not MsgReceived and not canceled do Application.ProcessMessages;
        if canceled then exit;
        if dodelete then Delete(j);
      end;
      IpSmtpClient1.Quit;
    end;
    Quit;
    if TotalMessages > 0 then begin
      if NumMessages > xNumMsg then Application.MessageBox(
        PChar('Processed: '+Long2StrL(TotalMessages)+' messages, '
        +Long2StrL(TotalAttachments)+' attachments.'#13
        +Long2StrL(NumMessages-xNumMsg)+' messages left on server.'),'',MB_OK)
      else Application.MessageBox(PChar('Processed: '+Long2StrL(TotalMessages)
        +' messages, '+Long2StrL(TotalAttachments)+' attachments'),'',MB_OK);
    end
    else Application.MessageBox(PChar('No Messages waiting for '+MailBoxId),
      '',MB_OK);
    Application.Terminate;
  end;
end;

procedure TForm1.SetOptions(Sender: TObject);
begin
  with Form2 do begin
    Changed := false;
    Edit1.Text := GetProfile('UserID','');
    Edit2.Text := GetProfile('Password','');
    Edit3.Text := GetProfile('ForwardTo','');
    Edit5.Text := GetProfile('CopyAttachPath','');
    Edit7.Visible := false;
    Label7.Caption := '';
    {$ifdef clubrep}
//    Edit7.Text := GetProfile('BadAttachPath','');
    Edit4.Visible := false;
    Edit6.Visible := false;
    Label4.Caption := '';
    Label6.Caption := '';
    Edit8.Text := GetProfile('BadMailTo','');
    {$ifdef second}
    Edit9.Text := GetProfile('SecAttachPath','');
    {$else}
    Edit9.Visible := false;
    Label9.Caption := '';
    {$endif}
    {$else}
    Edit4.Text := GetProfile('AttachFilePath','');
    Edit6.Text := GetProfile('LogFileName','');
    Edit8.Visible := false;
    Edit9.Visible := false;
    Label8.Caption := '';
    Label9.Caption := '';
    {$endif}
    CheckBox1.Checked := GetProfile('DeleteOnRead','0') = '1';
    CheckBox2.Checked := GetProfile('Reply','1') = '1';
    ShowModal;
    if Changed then begin
      SetProfile('UserID',Pchar(Edit1.Text));
      SetProfile('Password',Pchar(Edit2.Text));
      SetProfile('ForwardTo',Pchar(Edit3.Text));
      SetProfile('CopyAttachPath',Pchar(Edit5.Text));
      {$ifdef clubrep}
//      SetProfile('BadAttachPath',Pchar(Edit7.Text));
      SetProfile('BadMailTo',Pchar(Edit8.Text));
      {$ifdef second}
      SetProfile('SecAttachPath',Pchar(Edit9.Text));
      {$endif}
      {$else}
      SetProfile('AttachFilePath',Pchar(Edit4.Text));
      SetProfile('LogFileName',Pchar(Edit6.Text));
      {$endif}
      if CheckBox1.Checked then SetProfile('DeleteOnRead','1')
      else SetProfile('DeleteOnRead','0');
      if CheckBox2.Checked then SetProfile('Reply','1')
      else SetProfile('Reply','0');
    end;
  end;
end;

procedure TForm1.ProcessMessage(Client: TIpCustomPop3Client;
  Message: TIpMailMessage);
const
     MaxAtt = 100;
type
    Tbool100 = Array[0..MaxAtt] of boolean;
var
   {$ifdef clubrep}
   iscreditcard   : Tbool100;
   isbadclub      : Tbool100;
   ischeck        : Tbool100;
   Validfile      : Tbool100;
   ValidClub      : Tbool100;
   corrupt        : Tbool100;
   BadMP          : Tbool100;
   FinProcessed   : Tbool100;
   CruiseClub     : Tbool100;
   MPClubBad      : Array[0..MaxAtt] of byte;
   checkamount    : Array[0..MaxAtt] of integer;
   MonthYear      : Array[0..MaxAtt] of String;
   AVersion       : Array[0..MaxAtt] of word;
   {$ifdef second}
   SecondRep      : Tbool100;
   {$endif}
   anyvalid       : boolean;
   isbad  : boolean;
   {$else}
   AttRenamed     : Tbool100;
   {$endif}
   Winmail        : Tbool100;
   Msg            : TIpMimeEntity;
   isuu   : boolean;
   MsgLines : integer;

{$ifdef clubrep}
function TestPrevProcessed(const FinFileName: String; const Count: integer;
          var bool100: Tbool100; const isfin: boolean): boolean;
var
   Fname  : string;
   club   : string;
   cline  : String;
   cyear  : String;
   cmonth : String;
   iryear : word;
   irmonth : word;
   clubok : boolean;
   j      : integer;
   cftype : String;
   ryear  : char;
   rmonth : String;
   ProcessedF      : TextFile;
begin
  if Pos('\',FinFileName) > 0 then Fname := JustFileNameL(FinFileName)
  else Fname := FinFileName;
  club := Copy(Fname,2,6);
  if not Valid_club(club,true) then exit;
  j := Pos('.',Fname);
  if j < 8 then exit;
  cftype := Copy(Fname,j+1,3);
  if Length(cftype) <> 3 then exit;
  ryear := cftype[1];
  rmonth := Copy(cftype,2,2);
  iryear := ((Year div 10) * 10) + Ival(ryear);
  irmonth := Ival(rmonth);
  if iryear > Year then Dec(iryear,10);
  MonthYear[Count] := ' ('+MonthName(irmonth)+', '+Long2StrL(iryear)+')';
  CruiseClub[Count] := false;
  if isfin then begin
    if not ProcessedFinOK then exit;
    AssignFile(ProcessedF,FinProcessedFile);
  end;
  {else begin
    if not ProcessedMPOK then exit;
    AssignFile(ProcessedF,MPProcessedFile);
  end;}
  {$I-}
  Reset(ProcessedF); {$I+}
  if IOResult <> 0 then exit;
  cline := '000000';
  while not Eof(ProcessedF) and (Copy(cline,1,6) < club) do
    ReadLn(ProcessedF,cline);
  clubok := false;
  while not Eof(ProcessedF) and not clubok and (Copy(cline,1,6) = club) do begin
    cyear := GetCSVField(2,cline);
    cmonth := GetCSVField(3,cline);
    clubok := (ryear = cyear[4]) and (Ival(rmonth) = Ival(cmonth));
    CruiseClub[Count] := (GetCSVField(4,cline) = 'C');
    if not clubok then ReadLn(ProcessedF,cline);
  end;
  CloseFile(ProcessedF);
  bool100[Count] := clubok;
  Result := CruiseClub[Count];
end;

procedure TestClubFinReport(const FinFileName: String; const Count: integer);
const
     CheckFname = 'E:\DEPT\550\ACBLSCOR\CLUBREP\MAIL\CHECK.TXT';
     NotAllowedFname = 'E:\DEPT\550\ACBLSCOR\CLUBREP\MAIL\BADCLUB.TXT';
var
   FS             : TFileStream;
   TS             : TStAnsiTextStream;
   FinRecord      : String[254];
   ClubRecord     : TClubFinRec;
   j              : integer;
   eofx            : boolean;


function BadClub(const club: string):boolean;
var
   CheckF : TextFile;
   cline  : string;
begin
  if FileExists(NotAllowedFName) then begin
    AssignFile(CheckF,NotAllowedFName);
    Reset(CheckF);
    cline := 'xx';
    repeat
      ReadLn(CheckF,cline);
    until Eof(CheckF) or (cline=club);
    Result := cline=club;
    CloseFile(CheckF);
  end
  else Result := false;
end;

function isCheckClub(const club: string):boolean;
var
   CheckF : TextFile;
   cline  : string;
begin
  if FileExists(CheckFName) then begin
    AssignFile(CheckF,CheckFName);
    Reset(CheckF);
    cline := 'xx';
    repeat
      ReadLn(CheckF,cline);
    until Eof(CheckF) or (cline=club);
    Result := cline=club;
    CloseFile(CheckF);
  end
  else Result := false;
end;

begin
  FS := TFileStream.Create(FinFileName,fmOpenRead);
  TS := TStAnsiTextStream.Create(FS);
  for j := 1 to 2 do begin
    eofx := TS.AtEndOfStream;
    if not eofx then begin
      FinRecord := TS.ReadLine;
      Move(FinRecord,ClubRecord,SizeOf(TClubFinRec));
    end;
    if not eofx then with ClubRecord do case j of
      1: if ClubType = 'W' then begin
        validfile[Count] := true;
        ValidClub[Count] := valid_club(ChrStr(@ClubNum,6),true);
        AnyValid := true;
      end;
      2: begin
        isbadclub[Count] := BadClub(ChrStr(@ClubNum,6));
        if not isbadclub[Count] then begin
          checkamount[Count] := ChrNum(@TotalFees,6);
          iscreditcard[Count] := ((ChrNum(@PaymentType,1) = 1) or (checkamount[Count] = 0));
          if iscreditcard[Count] then begin
            ischeck[Count] := true;
            iscreditcard[Count] := true;
          end;
        end;
      end;
    end;
  end;
  TS.Free;
  FS.Free;
  CruiseClub[Count] := TestPrevProcessed(FinFileName,Count,FinProcessed,true);
  DeleteFile(FinFileName);
end;
{$endif}

function TestFileName(const AttachName: string): boolean;
var
   {$ifdef clubrep}
   SYear   : String;
   CPref   : String;
   SMonth  : String;
   {$ifdef second}
   SR      : TSearchRec;
   SError  : integer;
   {$endif}
   {$else}
   NewName     : string;
   {$endif}
begin
  with AttOldName do begin
    TestFileName := false;
    if UpperCase(AttachName) = Winmailname then Winmail[Count] := true
    {$ifndef anyattach}
    else if UpperCase(JustExtensionL(AttachName)) <> AttTypes[1] then exit;
    {$endif};
    if (Length(AttachName) > 0) then begin
      AttOldName.Add(AttachName);
      {$ifndef clubrep}
      AttNewName.Add(AttachName);
      {$endif}
    end;
    {$ifdef clubrep}
    if (Length(AttachName) > 11) and not Winmail[Count-1] then begin
      SYear := '1'+AttachName[7];
      CPref := Copy(AttachName,1,2);
      case UpCase(AttachName[8]) of
        'A' : SMonth := '10';
        'B' : SMonth := '11';
        'C' : SMonth := '12';
        else SMonth := '0'+AttachName[8];
      end;
      if (Length(SYear) = 2) and IsNumber(SYear) and IsNumber(CPref)
        and IsNumber(SMonth) then begin
        if Length(CPref) <> 2 then CPref := 'XX';
        CPref := CPref+'xxxx';
        SYear := '20'+SYear;
        CreateDirAttr(ClubSavePath+SYear,faReadOnly);
        CreateDirAttr(ClubSavePath+SYear+'\'+SMonth,faReadOnly);
        CreateDirAttr(ClubSavePath+SYear+'\'+SMonth+'\'+CPref,faReadOnly);
        {$ifdef second}
        SError := FindFirst(ClubSavePath+SYear+'\'+SMonth+'\'+CPref+'\'+AttachName,
          faAnyFile,SR);
        FindClose(SR);
        if SError = 0 then SecondRep[Count-1] := true;
        {$endif}
      end;
    end;
    TestFileName := true;
    Inc(TotalAttachments);
    {$else}
    if GetUniqueName(AttachPath,AttachName,NewName) then begin
      if NewName <> AttachName then begin
        Renamefile(AttachPath+AttachName,AttachPath+NewName);
        AttRenamed[Count-1] := true;
        AttNewName[Count-1] := NewName;
      end;
      TestFileName := true;
      Inc(TotalAttachments);
    end;
    {$endif}
  end;
end;

procedure TestCopySave(const AttachName: string);
var
   NewName  : String;
begin
  if Winmail[AttOldName.Count-1] then begin
    {$ifdef clubrep}
//    isbad := true;
    PCopyFile(PChar(AttachPath+AttachName),
      PChar(BadAttachPath+AttachName),false,canceled,true);
    if canceled then exit;
    {$endif}
    DeleteFile(AttachPath+AttachName);
  end
  else if (Length(CopyAttachPath) > 1)
    and GetUniqueName(CopyAttachPath,AttachName,NewName) then begin
    if NewName <> AttachName then
        RenameFile(CopyAttachPath+AttachName,CopyAttachPath+NewName);
    PCopyFile(PChar(AttachPath+AttachName),
      PChar(CopyAttachPath+AttachName),false,canceled,true);
  end;
  {$ifdef clubrep}
  if MPClubBad[AttOldName.Count-1] = 3 then isbad := true;
  {$endif}
end;

{$ifdef clubrep}
procedure TestLZHFile(const AttachName: String);
var
   j,k      : integer;
   zerolength : boolean;
   SR      : TSearchRec;
   LFname  : string;
begin
  if UpperCase(JustExtensionL(AttachName)) = AttTypes[1] then
    with AttOldName,CompLHA1 do begin
    if FindFirst(AttachPath+AttachName,faAnyFile,SR) = 0 then
      zerolength := SR.Size < 10
    else zerolength := true;
    FindClose(SR);
    corrupt[Count-1] := zerolength;
    if not zerolength then begin
      ArchiveName := AttachPath+AttachName;
      try
        Scan;
        TargetPath := AttachPath;
        for j := 0 to FileList.Count-1 do begin
          k := Pos('.',FileList[j]);
          if k > 8 then LFname := Copy(FileList[j],k-7,20)
          else LFname := FileList[j];
          case LFname[1] of
            'N','A': begin
              FilesToProcess.Clear;
              FilestoProcess.Add(FileList[j]);
              Verify;
            end;
            'M': begin
              FilesToProcess.Clear;
              FilestoProcess.Add(FileList[j]);
              Expand;
              if FilesProcessed.Count > 0 then begin
                MPClubBad[Count-1] := TestClubFile(FilesProcessed[0],aversion[Count-1]);
                CruiseClub[Count] := TestPrevProcessed(FilesProcessed[0],Count-1,FinProcessed,true);
                BadMP[Count-1] := (MPClubBad[Count-1] in [1,2]);
                DeleteFile(FilesProcessed[0]);
              end;
              validfile[Count-1] := true;
              ValidClub[Count-1] := valid_club(Copy(LFname,2,6),true);
              AnyValid := true;
            end;
            'F': begin
              FilesToProcess.Clear;
              FilestoProcess.Add(FileList[j]);
              Expand;
              if FilesProcessed.Count > 0 then begin
                TestClubFinReport(FilesProcessed[0],Count-1);
                DeleteFile(FilesProcessed[0]);
              end;
            end;
          end;
        end;
        except
          corrupt[Count-1] := true;
      end;
    end;
    if checkamount[Count-1] < 1 then MPClubBad[Count-1] := 0;
    if CruiseClub[Count-1] then FinProcessed[Count-1] := false;
    if iscreditcard[Count-1] or not validfile[Count-1] or Winmail[Count-1]
      or isbadclub[Count-1] or not ValidClub[Count-1] or BadMP[Count-1] then begin
      isbad := true;
      if not zerolength then PCopyFile(PChar(AttachPath+AttachName),
        PChar(BadAttachPath+AttachName),false,canceled,true);
      if canceled then exit;
      DeleteFile(AttachPath+AttachName);
    end
    else if FinProcessed[Count-1] then DeleteFile(AttachPath+AttachName)
    else if CruiseClub[Count-1] then DeleteFile(AttachPath+AttachName)
    {$ifdef second}
    else if SecondRep[Count-1] then begin
//      isbad := true;
      {if not zerolength then begin
        if Length(SecAttachPath) > 0 then
          PCopyFile(PChar(AttachPath+AttachName),
          PChar(SecAttachPath+AttachName),false,canceled,true)
        else if Length(BadAttachPath) > 0 then
          PCopyFile(PChar(AttachPath+AttachName),
          PChar(BadAttachPath+AttachName),false,canceled,true);
        if canceled then exit;
      end;}
      DeleteFile(AttachPath+AttachName);
    end
    {$endif}
    else TestCopySave(AttachName);
  end
  else TestCopySave(AttachName);
end;
{$endif}

procedure TestZipFile(const ZipName: string);
var
   j      : integer;
   AttachName       : String;
   fsize            : integer;
   F                : file;
begin
  AssignFile(F,AttachPath+ZipName);
  ReSet(F,1);
  fsize := FileSize(F);
  CloseFile(F);
  if fsize > 200 then with ZipMaster1 do begin
    ZipFileName := AttachPath+ZipName;
    ExtrBaseDir := JustPathNameL(AttachPath);
    for j := 0 to Count-1 do with ZipDirEntry(ZipContents[j]^) do begin
      AttachName := JustFileNameL(FileName);
      FSpecArgs.Clear;
      FSpecArgs.Add(FileName);
      if TestFileName(AttachName) then begin
        Extract;
        {$ifdef clubrep}
        TestLZHFile(AttachName);
        {$else}
        TestCopySave(AttachName);
        {$endif}
      end;
      if canceled then exit;
    end;
  end;
  DeleteFile(AttachPath+ZipName);
end;

procedure SendTheMailMessage(const isreply: boolean; const Addr: String);

procedure ReportAttach;
var
   at     : integer;
{$ifdef clubrep}
   mvfile : textfile;
   sver   : String;
   mver   : word;
   rver   : word;
   srver  : String;
   dver   : String;

procedure DownloadInst;
var
   smver : String;
begin
  smver := Real2StrL(mver/100.0,5,2);
  with IpSmtpClient1.Message.body do begin
    WriteLine(' ');
    WriteLine('You submitted a club report using ACBLscore version'+smver+'.');
    WriteLine('It is recommended that you download and install the latest version');
    WriteLine('of ACBLscore using one of the following links:');
    WriteLine('DOS version: http://web2.acbl.org/acblscore/ACBL'+dver+'U.exe');
    WriteLine('Windows version: http://web2.acbl.org/acblscore/ACBL'+dver+'W.exe');
    WriteLine(' ');
  end;
end;

procedure IncludeEmailInst;
begin
  with IpSmtpClient1.Message.body do begin
    WriteLine('Club reports submitted via email must be produced by');
    WriteLine('ACBLscore version '+srver+' or newer.');
    WriteLine('Detailed instructions can be found at');
    WriteLine('http://www.acbl.org/clubs_page/acblscore/submit-a-club-report');
  end;
end;

procedure IncludePayInst;
begin
  with IpSmtpClient1.Message.body do begin
    WriteLine('The report for club '+AttOldName[at]+' has been verified.');
    WriteLine('Follow the link below to open our secure paysite in your browser.');
    WriteLine('');
    WriteLine(PaySite+'?ClubNo='+AttOldName[at]+'club number variable'+'$ReportDate='+MonthYear[at]);
    WriteLine('');
    WriteLine('If your browser does not open, open it manually then copy');
    WriteLine('the paysite URL into the browsers address bar.');
  end;
end;
{$endif}

procedure Winmsg;
begin
  with IpSmtpClient1.Message.Body do begin
    WriteLine(AttOldName[at]+' REJECTED.  Winmail.dat files cannot be accepted.');
    WriteLine('Your Outlook mail program needs to be configured to disable');
    WriteLine('creating winmail.dat as an attachment.  Use the following link');
    WriteLine('for instructions to configure Outlook:');
    WriteLine('http://support.microsoft.com/default.aspx?scid=KB;EN-US;Q197064&');
  end;
end;

begin
  IpSmtpClient1.Message.Body.WriteLine(' ');
  {$ifdef clubrep}
  AssignFile(mvfile,'minaver.txt');
  {$I-}ReSet(mvfile);{$I+}
  if IOResult = 0 then begin
    ReadLn(mvfile,sver);
    ReadLn(mvfile,dver);
    CloseFile(mvfile);
    rver := Ival(sver);
    srver := sver[1]+'.'+Copy(sver,2,3);
  end
  else begin
    rver := 740;
    srver := '7.40';
  end;
  {$endif}
  with AttOldName do if Count > 0 then begin
    if Count > 1 then IpSmtpClient1.Message.Body.WriteLine(
      'The following files have been received:')
    else IpSmtpClient1.Message.Body.WriteLine(
      'The following file has been received:');
    IpSmtpClient1.Message.Body.WriteLine(' ');
    {$ifdef clubrep}
    mver := 1000;
    {$endif}
    for at := 0 to Count-1 do with IpSmtpClient1.Message.Body do begin
      {$ifdef clubrep}
      if aversion[at] < 500 then sver := ''
      else begin
        if aversion[at] < mver then mver := aversion[at];
        sver := ' v'+Real2StrL(aversion[at]/100.0,4,2)+' ';
      end;
      {$endif}
      if Winmail[at] then Winmsg
      {$ifdef clubrep}
      else if corrupt[at] then begin
        WriteLine(AttOldName[at]
          +' REJECTED.  This file is corrupt and could not be examined.');
        WriteLine('Generate and email the report again.  The majority of corrupt');
        WriteLine('files are due to incorrectly configured email software.');
      end
      else if not validfile[at] then begin
        if not anyvalid then begin
          WriteLine(AttOldName[at]
            +' REJECTED.  This file is not a valid club monthly report.');
          IncludeEmailInst;
        end;
      end
      else if not ValidClub[at] then begin
        WriteLine(AttOldname[at]
          +' REJECTED.  The club number used for this report is not valid.');
        IncludeEmailInst;
      end
      else if iscreditcard[at] then begin
        WriteLine(AttOldName[at]+MonthYear[at]
          +' REJECTED.  This report has credit card information.');
        WriteLine('As of October 15th 2015, ACBL can no longer accept credit');
        WriteLine('cards by email.  Generate the report again and select pay');
        WriteLine('by check. You will recieve an email with payment options.');
      end
      else if isbadclub[at] then begin
        WriteLine(AttOldName[at]+MonthYear[at]
          +' REJECTED.  This club is not allowed to submit club reports');
        WriteLine('until money owed for previous monthly reports is resolved.');
        WriteLine('Contact the ACBL Club Department for more information.');
      end
      else if BadMP[at] then begin
        WriteLine(AttOldName[at]+MonthYear[at]
          +' REJECTED.  The Masterpoint report in this file is not valid.');
        WriteLine('The ACBLscore data base may be damaged.  In ACBLscore, go into');
        WriteLine('Data Base, then Maintenance, then #6 Repair damaged data base.');
        WriteLine('Then generate the monthly report again.');
      end
      {$ifdef second}
      else if SecondRep[at] then begin
        WriteLine(AttOldName[at]+MonthYear[at]
          +' REJECTED.  This club report has previously been processed.');
        WriteLine('If this is a correction to your previous report, the monthly');
        WriteLine('report needs to be emailed to clubreport.comment@acbl.org');
      end
      {$endif}
      else begin
        if FinProcessed[at] then begin
//          if FinProcessed[at] and MPProcessed[at] then
          WriteLine(AttOldName[at]+MonthYear[at]
            +' REJECTED.  This club report has previously been processed.');
          WriteLine('If this is a correction to your previous report, the monthly');
          WriteLine('report needs to be emailed to clubreport.comment@acbl.org.');
        end
        else if CruiseClub[at] then begin
          WriteLine(AttOldName[at]+MonthYear[at]
            +' REJECTED.  This is a Cruise club.');
          WriteLine('Cruise club reports need to be emailed to cruisepoints@acbl.org');
        end
        else begin
          WriteLine(AttOldName[at]+sver+MonthYear[at]+' has been submitted for processing.');
//        if this club is in the pay by check file, tell them where to sent it.
          if ischeck[at] then begin
            WriteLine('Please submit $'+Real2StrL(checkamount[at] / 100,4,2)
              +' with the printed cover sheet to ACBL.');
          end
//        everybody else send them the paysite link
          else begin
          IncludePayInst
          end
        end;
      end;
      {$else}
      else WriteLine(AttOldName[at]);
      {$endif}
      WriteLine(' ');
    end;
    {$ifdef clubrep}
    if mver < rver then DownLoadInst;
  end
  else with IpSmtpClient1.Message.Body do begin
    WriteLine('No reports were found in your email message.');
    IncludeEmailInst;
    WriteLine(' ');
    {$endif}
  end;
end;

var
   j      : integer;
   at     : integer;
   FS             : TFileStream;
   TS             : TStAnsiTextStream;
   RepLine        : String;
   anybad         : boolean;
begin
  with Message do begin
    if isreply and (AttOldName.Count < 1)
      and ((ValidEmailAddress(ReplyTo,MaxEmailLen) < 0)
      or (ValidEmailAddress(From,MaxEmailLen) < 0)) then exit;
    if isreply and (ValidEmailAddress(ReplyTo,MaxEmailLen) = 0)
      and (ValidEmailAddress(From,MaxEmailLen) = 0) then exit;
    if Pos(UpperCase(MailBoxId),UpperCase(ReplyTo)) > 0 then exit;
    if Pos(UpperCase(MailBoxId),UpperCase(From)) > 0 then exit;
    IpSmtpClient1.Message.NewMessage;
    if isreply then begin
      {$ifdef clubrep}
      with AttOldName do if Count > 0 then begin
        anybad := false;
        for at := 0 to Count-1 do begin
          anybad := anybad or Winmail[at];
          anybad := anybad or corrupt[at];
          anybad := anybad or (not validfile[at] and not anyvalid);
          anybad := anybad or not ValidClub[at];
          anybad := anybad or iscreditcard[at];
          anybad := anybad or isbadclub[at];
          anybad := anybad or BadMP[at];
          anybad := anybad or FinProcessed[at];
          anybad := anybad or CruiseClub[at];
          {$ifdef second}
          anybad := anybad or SecondRep[at];
          {$endif}
        end;
        if not anybad then
          IpSmtpClient1.Message.Subject := AttOldName[0]+' report received ok'
        else for at := 0 to Count-1 do if Winmail[at] or corrupt[at]
          or (not validfile[at] and not anyvalid) or not ValidClub[at]
          or iscreditcard[at] or BadMP[at]
          or FinProcessed[at] or CruiseClub[at] or isbadclub[at]
          {$ifdef second}
          or SecondRep[at]
          {$endif}
          then begin
          IpSmtpClient1.Message.Subject := AttOldName[at]
            +' REJECTED - ACTION REQUIRED';
          Break;
        end;
      end
      else IpSmtpClient1.Message.Subject := 'No ACBL reports attached';
      {$else}
      IpSmtpClient1.Message.Subject := 'Re: '+Subject;
      {$endif}
      if (ValidEmailAddress(ReplyTo,MaxEmailLen) > 0) then
        IpSmtpClient1.Message.Recipients.Add(ReplyTo)
      else if (ValidEmailAddress(From,MaxEmailLen) <> 0) then
        IpSmtpClient1.Message.Recipients.Add(From)
      else exit;
    end
    else begin
      IpSmtpClient1.Message.Subject := 'Re: '+Subject;
      IpSmtpClient1.Message.Recipients.Add(Addr);
    end;
    {$ifdef usedeadbox}
    IpSmtpClient1.Message.From := DeadBox+'@'+EmailSuff;
    {$else}
    IpSmtpClient1.Message.From := MailBoxId+'@'+EmailSuff;
    {$endif}
    if Length(ReplyTo) > 0 then IpSmtpClient1.Message.ReplyTo := ReplyTo
    else IpSmtpClient1.Message.ReplyTo := From;
    if isreply then begin
      if Length(ReplyTextFile) > 0 then begin
        FS := TFileStream.Create(ReplyTextFile,fmOpenRead);
        TS := TStAnsiTextStream.Create(FS);
        while not TS.AtEndOfStream do begin
          RepLine := TS.ReadLine;
          if (Length(RepLine) > 1) and (Copy(RepLine,1,2) = '!1') then ReportAttach
          else IpSmtpClient1.Message.body.WriteLine(RepLine);
        end;
        TS.Free;
        FS.Free;
      end
      else begin
        IpSmtpClient1.Message.body.WriteLine(
          'Thank you.  Your message has been received.');
        ReportAttach;
      end;
    end
    else begin
      if (ValidEmailAddress(ReplyTo,MaxEmailLen) > 0) then
        IpSmtpClient1.Message.Body.WriteLine(
        'Message from '+ReplyTo+' has been received.')
      else IpSmtpClient1.Message.Body.WriteLine(
        'Message from '+From+' has been received.');
      IpSmtpClient1.Message.Body.WriteLine(' ');
      with AttOldName do if Count > 0 then begin
        IpSmtpClient1.Message.Body.WriteLine(
          'The following attachments have been received:');
        IpSmtpClient1.Message.Body.WriteLine(' ');
        for at := 0 to Count-1 do with IpSmtpClient1.Message.Body do begin
          if winmail[at] then WriteLine(
            AttOldName[at]+' rejected - WINMAIL.DAT file')
          {$ifdef clubrep}
          else if corrupt[at] then WriteLine(
            AttOldName[at]+' rejected - corrupt file')
          else if not validfile[at] then begin
            if not anyvalid then WriteLine(
              AttOldName[at]+' rejected - not a valid club report');
          end
          else if not ValidClub[at] then WriteLine(
            AttOldName[at]+' rejected - invalid club number')
          else if iscreditcard[at] then WriteLine(
            AttOldName[at]+' rejected - credit card information')
          else if isbadclub[at] then WriteLine(
            AttOldName[at]+' rejected - not allowed to submit reports')
          else if BadMP[at] then WriteLine(
            AttOldName[at]+' rejected - bad masterpoint report')
          else if FinProcessed[at] then WriteLine(
            AttOldName[at]+' rejected - second report')
          else if CruiseClub[at] then WriteLine(
            AttOldName[at]+' rejected - Cruise club')
          {else if FinProcessed[at] then WriteLine(
            AttOldName[at]+' - second financial report')
          else if MPProcessed[at] then WriteLine(
            AttOldName[at]+' - second masterpoint report')}
          else if MPClubBad[at] = 3 then WriteLine(
            AttOldName[at]+' - zero masterpoints reported - please check report')
          {$ifdef second}
          else if SecondRep[at] then WriteLine(
            AttOldName[at]+' rejected - second report')
          {$endif}
          else
          {$else}
          else if AttRenamed[at] then WriteLine(
            AttOldName[at]+': Old file renamed to '+AttNewName[at])
          else
          {$endif}
            WriteLine(AttOldName[at]);
        end;
      end
      else IpSmtpClient1.Message.Body.WriteLine('No attachments in the message');
      IpSmtpClient1.Message.Body.WriteLine(' ');
    end;
    for j := 0 to BodyText.Count-1 do
      IpSmtpClient1.Message.Body.WriteLine('>'+BodyText[j]);
  end;
  with IpSmtpClient1 do begin
    while State <> ssNoOp do Application.ProcessMessages;
    if canceled then exit;
    MsgSent := false;
    UserID := MailBoxId;
    Password := GetProfile('Password','');
    EnableAuth := true;
    SendMail(SmtpServer,false);
    while not MsgSent and not canceled do Application.processMessages;
  end;
end;

procedure TestForUUEncoding;
const
     TempFileName = 'UUDECODE.TMP';
var
   j      : integer;
   DecodeName : String;
   SaveName   : String;
begin
  isuu := false;
  for j := 0 to BodyText.Count-1 do if Copy(BodyText[j],1,7) = 'begin 6' then begin
    isuu := true;
    MsgLines := j;
    break;
  end;
  if isuu then begin
    Message.SaveToFile(AttachPath+TempFileName);
    j := 1;
    while UUDecode(AttachPath+TempFilename,DecodeName,SaveName,j) do begin
      if (UpperCase(JustExtensionL(DecodeName)) = 'ZIP') then
        TestZipFile(Decodename)
      else begin
        AttOldName.Add(DecodeName);
        {$ifndef clubrep}
        AttNewName.Add(SaveName);
        AttRenamed[AttOldName.Count-1] := DecodeName <> SaveName;
        {$endif}
        Inc(j);
        {$ifdef clubrep}
        TestLzhFile(DecodeName);
        {$else}
        TestCopySave(DecodeName);
        {$endif}
      end;
    end;
    if canceled then exit;
    DeleteFile(AttachPath+TempFileName);
    with BodyText do for j := Count-1 downto MsgLines do Delete(j);
  end;
end;

procedure LogAttach;
var
   Logline  : String;

{$ifdef savenon}
procedure SaveMsg;
var
   Srec     : TSearchRec;
   Serr     : integer;
   at       : integer;
begin
    at := 0;
    Serr := 0;
    while (Serr = 0) and (at < 999) do begin
      Serr := FindFirst('Msg'+numcvt(at,3,true)+'.txt',faAnyFile,Srec);
      FindClose(Srec);
      if Serr = 0 then Inc(at);
    end;
    Message.SaveToFile('Msg'+numcvt(at,3,true)+'.txt');
    logline := logline+':  Saved as Msg'+numcvt(at,3,true)+'.txt';
end;
{$endif}

var
   at     : integer;
begin
  with AttOldName do if Count > 0 then for at := 0 to Count-1 do begin
    if winmail[at] then AddToLogFile('  '+AttOldName[at]
      +' - WINMAIL.DAT file',false)
    {$ifdef clubrep}
    else if corrupt[at] then begin
      logline := '  '+AttOldName[at]+' - corrupt';
      {$ifdef savebad}
      SaveMsg;
      {$endif}
      AddToLogFile(logline,false);
    end
    else if not validfile[at] then begin
      if not anyvalid then AddToLogFile('  '+AttOldName[at]
        +' - not a valid club report',false);
    end
    else if not ValidClub[at] then AddToLogFile('  '+AttOldName[at]
      +' - invalid club number',false)
    else if iscreditcard[at] then AddToLogFile('  '+AttOldName[at]
      +' - credit card information',false)
    else if isbadclub[at] then AddToLogFile('  '+AttOldName[at]
      +' - not allowed - money problems',false)
    else if BadMP[at] then AddToLogFile('  '+AttOldName[at]
      +' - bad masterpoint file',false)
    else if FinProcessed[at] then
      AddToLogFile('  '+AttOldName[at]+' - second report',false)
    else if CruiseClub[at] then
      AddToLogFile('  '+AttOldName[at]+' - Cruise club',false)
    {else if FinProcessed[at] then
      AddToLogFile('  '+AttOldName[at]+' - second financial report',false)
    else if MPProcessed[at] then
      AddToLogFile('  '+AttOldName[at]+' - second masterpoint report',false)}
    {$ifdef second}
    else if SecondRep[at] then AddToLogFile('  '+AttOldName[at]
      +' - second report',false)
    {$endif}
    else
    {$else}
    else if AttRenamed[at] then AddToLogFile('  '+AttOldName[at]
      +': Old file renamed to '+AttNewName[at],false)
    else
    {$endif}
      AddToLogFile('  '+AttOldName[at],false);
  end
  else begin
    logline := '  No attachments';
    {$ifdef savenon}
    SaveMsg;
    {$endif}
    AddToLogFile(logline,false);
  end;
end;

procedure CheckAttachSave(const MimePart: TIpMimeEntity);

function FixAttachName(const Aname: String): String;
var
   Qpos  : integer;
   lq    : integer;
   fq    : integer;
begin
  Result := JustFileNameL(Aname);
  repeat
    Qpos := Pos(' ',Result);
    if Qpos > 0 then Delete(Result,Qpos,1);
  until Qpos < 1;
  Qpos := Pos('?',Result);
  if Qpos < 1 then exit;
  lq := Length(Result);
  repeat
    if Result[lq] <> '?' then Dec(lq);
  until (Result[lq] = '?') or (lq < 2);
  if lq < 4 then exit;
  fq := lq-1;
  repeat
    if Result[fq] <> '?' then Dec(fq);
  until (Result[fq] = '?') or (fq < 1);
  Result := Copy(Result,fq+1,lq-fq-1);
end;

var
   AttachName     : String;
   at     : integer;
   aExt   : String;
begin
  with MimePart do begin
    with MimeParts do for at := 0 to Count-1 do
      with MimeParts[at] do CheckAttachSave(Parts[at]);
    if canceled then exit;
    AttachName := FixAttachName(EntityName);
    if Length(AttachName) < 1 then AttachName := FixAttachName(FileName);
    if (Length(AttachPath) < 1) {or not IsMime}
      or (Length(AttachName) < 1) or (AttachName[1] = '%') then exit;
    aExt := UpperCase(JustExtensionL(AttachName));
    if aExt = AttTypes[2] then begin
      ExtractBodyFile(AttachPath+AttachName);
      TestZipFile(AttachName);
    end
    else if TestFileName(AttachName) then begin
      ExtractBodyFile(AttachPath+AttachName);
      {$ifdef clubrep}
      TestLZHFile(AttachName);
      {$else}
      TestCopySave(AttachName);
      {$endif}
    end;
  end;
end;

var
   at     : integer;
begin
  {$ifdef clubrep}
  FillChar(CruiseClub,SizeOf(CruiseClub),false);
  FillChar(FinProcessed,SizeOf(FinProcessed),false);
  FillChar(iscreditcard,SizeOf(iscreditcard),true);
  FillChar(isbadclub,SizeOf(isbadclub),false);
  FillChar(ischeck,SizeOf(ischeck),false);
  FillChar(corrupt,SizeOf(corrupt),false);
  FillChar(BadMP,SizeOf(BadMP),false);
  FillChar(MPClubBad,SizeOf(MPClubBad),0);
  FillChar(validfile,SizeOf(validfile),false);
  FillChar(validClub,SizeOf(validClub),false);
  FillChar(AVersion,SizeOf(AVersion),0);
  FillChar(checkamount,SizeOf(checkamount),0);
  for at := 0 to MaxAtt do MonthYear[at] := '';
  {$ifdef second}
  FillChar(SecondRep,SizeOf(SecondRep),false);
  {$endif}
  anyvalid := false;
  isbad := false;
  {$else}
  FillChar(AttRenamed,SizeOf(AttRenamed),false);
  AttNewName.Clear;
  {$endif}
  ShowCopyError := true;
  ShowCmpErrors := false;
  FillChar(Winmail,SizeOf(Winmail),false);
  AttOldName.Clear;
  isuu := false;
  Inc(TotalMessages);
  Msg := nil;
  with Message do try
    {$ifdef test}
    Inc(MsgSaveN);
    SaveToFile('Msg'+numcvt(MsgSaveN,3,true)+'.eml');
    {$endif}
    {$ifdef UseFile}
    LoadFromFile('c:\temp\email\test6.eml');
    SaveToFile('test.eml');
    {$endif}
    BodyText.Clear;
    Msg := GetBodyPlain(false);
    if Assigned(Msg) then Msg.ExtractBodyStrings(BodyText)
    else begin
      ExtractBodyStrings(BodyText);
      TestForUUEncoding;
    end;
    if canceled then exit;
    with MimeParts do for at := 0 to Count-1 do
      with MimeParts[at] do CheckAttachSave(Parts[at]);
    if canceled then exit;
    if dolog then begin
      if (ValidEmailAddress(ReplyTo,MaxEmailLen) > 0) then begin
        AddToLogFile(ReplyTo,false);
        LogAttach;
      end
      else if (AttOldName.Count > 0) or (ValidEmailAddress(From,MaxEmailLen) > 0)
        then begin
        AddToLogFile(From,false);
        LogAttach;
      end;
    end;
//    if Length(ForwardTo) > 0 then SendTheMailMessage(false,ForwardTo);
    if doreply then SendTheMailMessage(true,'');
    {$ifdef clubrep}
    if (Length(BadMailTo) > 0) and isbad then SendTheMailMessage(false,BadMailTo);
    {$endif}
  except

  end;
  if Assigned(Msg) then Msg.Free;
  MsgReceived := true;
end;

procedure DoTheExit;
begin
  with Form1.IpPop3Client1 do if Connected then Quit;
  with Form1.IpSmtpClient1 do if Connected then Quit;
  Application.Terminate;
  canceled := true;
end;

procedure TForm1.Quit(Sender: TObject);
begin
  DoTheExit;
  ExitProcess(0);
end;

procedure TForm1.Pop3ServerResponse(Client: TIpCustomPop3Client;
  ResponseTo: TIpPop3States; const Code, Response: String);
begin
  Statusbar1.SimpleText := Code+' - '+Response;
  if Code = '-ERR' then begin
    Application.MessageBox(Pchar('ERROR: '+Response),'',MB_OK+MB_ICONSTOP);
    DotheExit;
  end;
end;

procedure TForm1.Pop3TaskComplete(Client: TIpCustomPop3Client;
  Task: TIpPop3Tasks; Response: TStringList);
begin
  case Task of
    ptLogon: ConnectComplete := true;
  end;
end;

procedure TForm1.MessageSent(Client: TIpCustomSmtpClient;
  var NextMsgReady: Boolean);
begin
  MsgSent := true;
  NextMsgReady := false;
end;

procedure TForm1.SmtpServerResponse(Client: TIpCustomSmtpClient;
  ResponseTo: TIpSmtpStates; Code: Integer; const Response: String);
begin
  Statusbar1.SimpleText := Long2StrL(Code)+' - '+Response;
  if (Code > 400) and (Code <> 502) then begin
    Application.MessageBox(Pchar('ERROR: '+Response),'',MB_OK+MB_ICONSTOP);
    DotheExit;
  end;
end;

procedure TForm1.ErrorReply(Sender: TObject; Handle: Cardinal;
  ErrCode: Integer; const ErrStr: String);
begin
  Application.MessageBox(Pchar('ERROR: '+Long2StrL(ErrCode)+': '+ErrStr),
    '',MB_OK+MB_ICONSTOP);
  DotheExit;
end;

procedure TForm1.MailClose(Sender: TObject; var Action: TCloseAction);
begin
  Quit(Sender);
end;

end.


