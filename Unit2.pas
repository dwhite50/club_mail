unit Unit2;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls;

type
  TForm2 = class(TForm)
    Label1: TLabel;
    Edit1: TEdit;
    Label2: TLabel;
    Edit2: TEdit;
    Label3: TLabel;
    Edit3: TEdit;
    CheckBox1: TCheckBox;
    Label4: TLabel;
    Edit4: TEdit;
    Label5: TLabel;
    Edit5: TEdit;
    Button1: TButton;
    Button2: TButton;
    CheckBox2: TCheckBox;
    Edit6: TEdit;
    Label6: TLabel;
    Edit7: TEdit;
    Label7: TLabel;
    Edit8: TEdit;
    Label8: TLabel;
    Edit9: TEdit;
    Label9: TLabel;
    procedure Cancelit(Sender: TObject);
    procedure Acceptit(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    Changed : boolean;
  end;

var
  Form2: TForm2;

implementation

{$R *.DFM}

procedure TForm2.Cancelit(Sender: TObject);
begin
  Close;
end;

procedure TForm2.Acceptit(Sender: TObject);
begin
  Changed := true;
  Close;
end;

end.
